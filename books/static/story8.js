$(document).ready(function() {
    $.ajax({
        url: '/books/collection?q=startup',
        success: function(data) {
            var array_items = data.items;
            console.log(array_items);
            $("#daftar_isi").empty();
            for (i = 0; i < array_items.length; i++) {
                var no = i + 1;
                var judul = array_items[i].volumeInfo.title;
                var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                var author = array_items[i].volumeInfo.authors;
                var tahun = array_items[i].volumeInfo.publishedDate;
                $('#daftar_isi').append("<tr><td>" + no + "</td><td><img src=" + gambar + "></td><td>" + judul + "</td><td>" + author + "</td><td>" + tahun + "</td></tr>")
            }
        }
    });
    $("#keyword").keyup( function() {
        var ketikan = $("#keyword").val();
        console.log(ketikan);

        // panggil dg teknik AJAX
        $.ajax({
            url: '/books/collection?q=' + ketikan,
            success: function(data) {
                var array_items = data.items;
                console.log(array_items);
                $("#daftar_isi").empty();
                for (i = 0; i < array_items.length; i++) {
                    var no = i + 1;
                    var judul = array_items[i].volumeInfo.title;
                    var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var author = array_items[i].volumeInfo.authors;
                    var tahun = array_items[i].volumeInfo.publishedDate;
                    $('#daftar_isi').append("<tr><td>" + no + "</td><td><img src=" + gambar + "></td><td>" + judul + "</td><td>" + author + "</td><td>" + tahun + "</td></tr>")
                }
            }
        });
    });
});
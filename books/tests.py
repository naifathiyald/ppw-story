from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from django.apps import apps
from books.apps import BooksConfig
from .views import index
import json, requests

# Create your tests here.
class Story7Test(TestCase):

    def test_books_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_books_using_index_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, index)
    
    def test_books_using_index_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books/index.html')
    
    def test_books_content_correct(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Books Collection', html_response)

    def test_books_apps(self):
        self.assertEqual(BooksConfig.name, 'books')
        self.assertEqual(apps.get_app_config('books').name, 'books')
    
    def test_books_json(self):
        response = Client().get('/books/collection?q=frozen')
        data = json.loads(response.content)
        self.assertEqual((data)['kind'], 'books#volumes')
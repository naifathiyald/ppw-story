from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'books/index.html')

def collection(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)
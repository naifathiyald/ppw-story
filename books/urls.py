from django.contrib import admin
from django.urls import path, include

# from .views import addActivity
from . import views

app_name = 'books'

urlpatterns = [
    path('collection', views.collection, name='collection'),
    path('', views.index, name='index'),
    
]

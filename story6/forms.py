from django import forms
# from .models import Activity, Person

# class ActivityForm(forms.ModelForm):
#     class Meta:
#         model = Activity
#         fields = [
#             'nama_kegiatan',
#             'deskripsi'
#         ]

# class PersonForm(forms.ModelForm):
#     kegiatan = forms.
#     class Meta:
#         model = Person
#         fields = [
#             'nama'
#         ]

class ActivityForm(forms.Form):
    nama_kegiatan = forms.CharField(
        label="Nama kegiatan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60',
            }
        )
    )

    deskripsi = forms.CharField(
        label="Deskripsi",
        max_length=1000,
        widget=forms.Textarea(
            
        )
    )


class PersonForm(forms.Form):
    nama = forms.CharField(
        label="Nama",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

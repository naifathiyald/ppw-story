from django.shortcuts import render, redirect
from .models import Activity, Person
from .forms import ActivityForm, PersonForm
from datetime import datetime

# Create your views here.
def index(request):
    return render(request, 'story6/index.html')

def addActivity(request):
    activity_form = ActivityForm()
    if request.method == "POST":
        activity_form_input = ActivityForm(request.POST)
        if activity_form_input.is_valid():
            data = activity_form_input.cleaned_data
            activity_input = Activity()
            activity_input.nama_kegiatan = data['nama_kegiatan']
            activity_input.deskripsi = data['deskripsi']
            activity_input.save()
            return redirect('story6:index')
        else:
            return render(request, 'story6/addactivity.html', {'form': activity_form, 'status': 'failed'})
    context = {
        'form': activity_form
    }
    return render(request, 'story6/addactivity.html', context)


def listActivity(request):
    activities = Activity.objects.all()
    persons = Person.objects.all()
    context = {
        'activities' : activities,
        'persons' : persons
    }
    return render(request, 'story6/listActivity.html', context)


def delete(request, delete_id):
    activity = Activity.objects.all()
    person = Person.objects.all()
    print(delete_id)
    person_to_delete = Person.objects.get(id=delete_id)
    person_to_delete.delete()
    
    return redirect('story6:listActivity')


def register(request, task_id):
    form_person = PersonForm()

    if request.method == "POST":
        form_person_input = PersonForm(request.POST)
        if form_person_input.is_valid():
            data = form_person_input.cleaned_data
            newPerson = Person()
            newPerson.nama = data['nama']
            newPerson.kegiatan = Activity.objects.get(id=task_id)
            newPerson.save()
            return redirect('story6:listActivity')
        else:
            return render(request, 'story6/register.html', {'form': form_person, 'status': 'failed'})
    else:
        return render(request, 'story6/register.html', {'form': form_person})


# Generated by Django 3.1.2 on 2020-10-24 11:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='deskripsi',
            field=models.CharField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='activity',
            name='nama_kegiatan',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='person',
            name='nama',
            field=models.CharField(max_length=64),
        ),
    ]

from django.db import models

# Create your models here.
class Activity(models.Model):
    nama_kegiatan = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=1000)

    def __str__(self):
        return self.nama_kegiatan

class Person(models.Model):
    nama = models.CharField(max_length=64)
    kegiatan = models.ForeignKey(
        Activity,
        on_delete=models.CASCADE, 
        null=True, 
        blank=True)

    def __str__(self):
        return self.nama

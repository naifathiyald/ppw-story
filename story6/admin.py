from django.contrib import admin
from .models import Person, Activity

# Register your models here.
admin.site.register(Person)
admin.site.register(Activity)
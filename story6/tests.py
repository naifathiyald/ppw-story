from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Activity, Person
from django.apps import apps
from .views import index, addActivity, listActivity, delete, register
from .forms import ActivityForm, PersonForm

# Create your tests here.
class TestModel(TestCase):
    def setUp(self):
        self.activity = Activity.objects.create(
            nama_kegiatan="gambar", deskripsi="gambar singa")
        self.person = Person.objects.create(nama="mark lee")

    def test_instance_created(self):
        self.assertEqual(Activity.objects.count(), 1)
        self.assertEqual(Person.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.activity), "gambar")
        self.assertEqual(str(self.person), "mark lee")


class TestForm(TestCase):

    def test_form_is_valid(self):
        activity_form = ActivityForm(data={
            "nama_kegiatan": "gambar",
            "deskripsi": "gambar singa"
        })
        self.assertTrue(activity_form.is_valid())
        person_form = PersonForm(data={
            'nama': "mark lee"
        })
        self.assertTrue(activity_form.is_valid())

    def test_form_invalid(self):
        activity_form = ActivityForm(data={})
        self.assertFalse(activity_form.is_valid())
        activity_form = PersonForm(data={})
        self.assertFalse(activity_form.is_valid())


class TestUrls(TestCase):

    def setUp(self):
        self.activity = Activity.objects.create(
            nama_kegiatan="gambar", deskripsi="gambar singa")
        self.person = Person.objects.create(
            nama="johnny", kegiatan=Activity.objects.get(nama_kegiatan="gambar"))
        self.index = reverse("story6:index")
        self.listActivity = reverse("story6:listActivity")
        self.addActivity = reverse("story6:addActivity")
        self.register = reverse("story6:register", args=[self.activity.pk])
        self.delete = reverse("story6:delete", args=[self.person.pk])

    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_listActivity_use_right_function(self):
        found = resolve(self.listActivity)
        self.assertEqual(found.func, listActivity)

    def test_addActivity_use_right_function(self):
        found = resolve(self.addActivity)
        self.assertEqual(found.func, addActivity)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, delete)


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("story6:index")
        self.listActivity = reverse("story6:listActivity")
        self.addActivity = reverse("story6:addActivity")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_GET_listActivity(self):
        response = self.client.get(self.listActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/listActivity.html')

    def test_GET_addActivity(self):
        response = self.client.get(self.addActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/addactivity.html')
# 
    def test_POST_addActivity(self):
        response = self.client.post(self.addActivity,
                                    {
                                        'nama_kegiatan': 'gambar',
                                        'deskripsi': "gambar singa"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addActivity_invalid(self):
        response = self.client.post(self.addActivity,
                                    {
                                        'nama_kegiatan': '',
                                        'deskripsi': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'story6/addactivity.html')

    def test_GET_delete(self):
        activity = Activity(nama_kegiatan="abc", deskripsi="CDF")
        activity.save()
        person = Person(nama="jisung",
                      kegiatan=Activity.objects.get(pk=1))
        person.save()
        response = self.client.get(reverse('story6:delete', args=[person.pk]))
        self.assertEqual(Person.objects.count(), 0)
        self.assertEqual(response.status_code, 302)



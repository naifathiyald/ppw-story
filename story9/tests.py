from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index, loginView, logoutView, register
from .models import Profile
from .forms import LoginForm, RegisterForm
from django.contrib.auth.models import User
from django.apps import apps
from .apps import Story9Config
from django.http import HttpRequest

# Create your tests here.

class TestUrls(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_url_is_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_logout_url_is_exist(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 200)

class TestForm(TestCase):
    def test_form_is_valid(self):
        form_login = LoginForm(data={
            "username": "mark",
            "password": "nct2020",
        })

        form_regist = RegisterForm(data={
            "username": "mark",
            "email": "mark@gmail.com",
            "password_first" : "nct2020",
            "password_again" : "nct2020",
        })
        self.assertTrue(form_regist.is_valid())

    def test_form_invalid(self):
        form_login = LoginForm(data={})
        self.assertFalse(form_login.is_valid())
        form_register = RegisterForm(data={})
        self.assertFalse(form_register.is_valid())

    def test_form_regist_is_exist(self):
        form_regist = RegisterForm(data={
            "username": "dejun",
            "email": "dejun@gmail.com",
            "password_first" : "nct2020",
            "password_again" : "nct2020",
        })
        form_regist = RegisterForm(data={
            "username": "dejun",
            "email": "dejun@gmail.com",
            "password_first" : "nct2020",
            "password_again" : "nct2020",
        })
        self.assertTrue(form_regist.is_valid())

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_view = reverse("story9:login")
        self.register_view = reverse("story9:register")
        self.logout_view = reverse("story9:logout")
        self.user_new = User.objects.create_user("mark","mark@gmail.com", password='nct2020')
        self.user_new.save()
        self.profile = Profile.objects.create(user=self.user_new)

    def test_GET_login(self):
        response = self.client.get(self.login_view, {
            'username': 'mark', 
            'password':'nct2020'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_GET_register(self):
        response = self.client.get(self.register_view, {
            'username': 'mark', 
            'email': 'mark@gmail.com', 
            'password_first':'nct2020', 
            'password_again':'nct2020'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/register.html')
    
    def test_GET_logout(self):
        response = self.client.get(self.logout_view)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/logout.html')

    def test_register_login_post(self):
        response = self.client.post(self.register_view, data = {
            "username" : "mark",
            "email" : "mark@gmail.com",
            "password_first" : "nct2020",
            "password_again" : "nct2020",
        })

        response = self.client.post(self.register_view, data = {
            "username" : "mark",
            "email" : "mark@gmail.com",
            "password_first" : "nct2020",
            "password_again" : "nct2002",
        })

        response = self.client.post(self.register_view, data = {
            "username" : "markAgain",
            "email" : "marknct2020@gmail.com",
            "password_first" : "nct2020",
            "password_again" : "nct2020",
        })

        response = self.client.post(self.login_view,data ={
            "username" : "mark",
            "password" : "nct2020"
        })
        self.assertEquals(response.status_code,302)

    def test_not_register_yet(self):
        response = self.client.post(self.login_view,data ={
            "username" : "markpro",
            "password" : "nct2020"
        })
        self.assertEquals(response.status_code,200)
        
class ModelTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("mark", password="nct2020")
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user
        )
        self.response = self.client.login(
            username = "mark",
            password = "nct2020"
        )

    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(),1)

    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user,self.new_user)

class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(Story9Config.name, 'story9') 
        self.assertEqual(apps.get_app_config('story9').name, 'story9')
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from .forms import LoginForm, RegisterForm

User = get_user_model()

# Create your views here.
def index(request):
    context = {
        'title': 'Welcome'
    }
    return render(request, 'story9/index.html', context)

def register(request):
    form = RegisterForm(request.POST or None)
    context = {
        'form': form,
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password_first")
        User.objects.create_user(username, email, password)
        return redirect('story9:login')

        
    return render(request, "story9/register.html", context)


def logoutView(request):
    context = {}
    logout(request)
    return render(request, 'story9/logout.html', context)

def loginView(request):
    form = LoginForm(request.POST or None)
    context = {
        'form': form
    }
    
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('story9:index')
        else:
            context["error"]= "Incorrect username or password"
            return render(request, 'story9/login.html', context)

    return render(request, "story9/login.html", context=context)

from django.contrib import admin
from django.urls import path, include

# from .views import addActivity
from . import views

app_name = 'story9'

urlpatterns = [
    path('login/', views.loginView, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logoutView, name='logout'),
    path('', views.index, name='index'),
]

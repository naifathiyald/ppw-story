from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from django.apps import apps
from story7.apps import Story7Config
from .views import index

# Create your tests here.
class Story7Test(TestCase):

    def test_story7_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_using_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)
    
    def test_story7_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html')
    
    def test_story7_content_correct(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Aktivitas Saat Ini', html_response)

    def test_story7_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')
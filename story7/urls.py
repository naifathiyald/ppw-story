from django.contrib import admin
from django.urls import path, include

# from .views import addActivity
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
]

from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('choose/<str:id>/', views.pilihjadwal, name='pilihjadwal'),
    path('delete/<int:id>/', views.delete, name='delete'),
    path('about/', views.about, name='about'),
    path('skills/', views.skills, name='skills'),
    path('experiences/', views.experiences, name='experiences'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('create/', views.create, name='create'),
    path('', views.index, name='index'),
]
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    jumlah_sks = models.IntegerField(validators=[MinValueValidator(1),
                                       MaxValueValidator(6)])
    deskripsi = models.TextField(blank=False, null=True)
    term = models.CharField(max_length=30)
    ruang_kelas = models.CharField(max_length=6)

    def __str__(self):
        return "{}.{}".format(self.id,self.nama_matkul)
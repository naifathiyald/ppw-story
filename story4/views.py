from django.shortcuts import render, redirect
from .forms import Input_Form
from .models import Matkul


# Create your views here.
def index(request):
    return render(request, 'story4/index.html')

def about(request):
    return render(request, 'story4/about.html')

def skills(request):
    return render(request, 'story4/skills.html')

def experiences(request):
    return render(request, 'story4/experiences.html')

def jadwal(request):
    posts = Matkul.objects.all()

    context = {
        'posts' : posts
    }

    return render(request, 'story4/jadwal.html', context)

def pilihjadwal(request,id):
    matkul = Matkul.objects.get(id=id)

    context = {
        'matkul' : matkul
    }

    return render(request, 'story4/pilihjadwal.html', context)

def create(request):
    form = Input_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()

            return redirect('story4:jadwal')

    context = {
        'form': form
    }
    return render(request, 'story4/create.html', context)

def delete(request, id):
    matkul = Matkul.objects.get(id=id)
    matkul.delete()
    return redirect('story4:jadwal')
from django import forms
from .models import Matkul

class Input_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama_matkul',
            'dosen',
            'jumlah_sks',
            'deskripsi',
            'term',
            'ruang_kelas'
        ]

